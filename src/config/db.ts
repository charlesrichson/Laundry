import mongoose from 'mongoose'
import config from './config'

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(config.mongodb_uri, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    })

    const { host, port, name } = conn.connection

    console.log(`MongoDB Connected: ${host}:${port}/${name}`)
  } catch (error) {
    console.error(error)
    process.exit(1)
  }
}

export default connectDB
