import { createOne, updateOne, getAll } from '../middleware/query'
import Settings from '../models/Settings'

// @desc       Get Settings
// @route      GET /api/v1/settings
// @access     Public
export const getSettings = getAll(Settings)

// @desc       Create Settings
// @route      POST /api/v1/settings
// @access     Private & Restricted
export const createSettings = createOne(Settings)

// @desc       Update Settings
// @route      PATCH /api/v1/settings
// @access     Private & Restricted
export const updateSettings = updateOne(Settings)
