import asyncHandler from 'express-async-handler'
import Attendance from '../models/Attendance'
import { getAll } from '../middleware/query'

// @desc       Create Attendance
// @route      POST /api/v1/attendance
// @access     Public
export const createAttendance = asyncHandler(async (req, res, next) => {
  if (!req.body.employee) {
    res.status(400)
    throw new Error('Please select an employee')
  }

  const { employee } = req.body

  const employeeAttendance = await Attendance.findOne({ employee })

  if (employeeAttendance) {
    res.status(400)
    throw new Error('User attendance already exist.')
  }

  const newAttendance = await Attendance.create(req.body)

  res.status(201).json({
    success: true,
    data: newAttendance,
  })
})

// @desc       Update Attendance
// @route      PATCH /api/v1/attendance/:employee
// @access     Public
export const updateAttendance = asyncHandler(async (req, res, next) => {
  if (!req.params.employee) {
    res.status(400)
    throw new Error('Please select and employee')
  }

  if (!req.body.date) {
    res.status(400)
    throw new Error('Please add a date.')
  }

  const {
    body: { date },
    params: { employee },
  } = req

  const employeeAttendance = await Attendance.findOne({ employee })

  if (!employeeAttendance) {
    res.status(400)
    throw new Error('User attendance does not exist.')
  }

  employeeAttendance.date = [...employeeAttendance.date, date]
  await employeeAttendance.save()

  res.status(200).json({
    success: true,
    data: employeeAttendance,
  })
})

// @desc       Delete Attendance
// @route      DELETE /api/v1/attendance
// @access     Private & Restricted
export const deleteAttendance = asyncHandler(async (req, res, next) => {
  await Attendance.deleteMany()

  res.status(204).json({
    success: true,
    data: null,
  })
})

// @desc       Get Attendance
// @route      GET /api/v1/attendance
// @access     Private & Restricted
export const getAttendance = getAll(Attendance)
