import { deleteOne, getAll, getOne, updateOne } from '../middleware/query'
import asyncHandler from 'express-async-handler'
import ExpoToken from '../models/ExpoToken'
import User from '../models/User'

// @desc       Create Expo Token
// @route      POST /api/v1/token
// @access     Protected
export const createToken = asyncHandler(async (req, res, next) => {
  const expoToken = await ExpoToken.create(req.body)

  await User.findByIdAndUpdate(
    req.user.id,
    { expoTokenId: expoToken._id },
    {
      runValidators: true,
    },
  )

  res.status(201).json({
    success: true,
    data: expoToken,
  })
})

// @desc       Update Expo Token
// @route      PATCH /api/v1/laundry/:id
// @access     Protected
export const updateToken = updateOne(ExpoToken)

// @desc       Delete Expo Token
// @route      DELETE /api/v1/laundry/:id
// @access     Protected
export const deleteToken = deleteOne(ExpoToken)

// @desc       Get Tokens
// @route      GET /api/v1/laundry
// @access     Protected && Restricted
export const getTokens = getAll(ExpoToken)

// @desc       Get Token
// @route      GET /api/v1/laundry/:id
// @access     Protected
export const getToken = getOne(ExpoToken)
