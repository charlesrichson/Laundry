import asyncHandler from 'express-async-handler'
import { createOne, getAll, getOne } from '../middleware/query'
import Message from '../models/Message'

// @desc       Create Message
// @route      POST /api/v1/message
// @access     Public
export const createMessage = createOne(Message)

// @desc       Get Messages
// @route      GET /api/v1/messages
// @access     Protected && Restricted
export const getMessages = getAll(Message)

// @desc       Get Message
// @route      GET /api/v1/messages/:id
// @access     Protected && Restricted
export const getMessage = getOne(Message)

// @desc       Delete All Messages
// @route      DELETE /api/v1/messages
// @access     Protected && Restricted
export const deleteMessages = asyncHandler(async (req, res, next) => {
  await Message.deleteMany()

  res.status(204).json({
    success: true,
    data: null,
  })
})
