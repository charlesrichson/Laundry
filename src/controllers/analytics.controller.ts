import { ILaundry, IUser, ICustomer } from './../utils/interface';
import asyncHandler from 'express-async-handler';
import moment, { Moment } from 'moment';
import Laundry from '../models/Laundry';
import User from '../models/User';
import Customer from '../models/Customer';
import UtilityService from '../utils/utils';

// @desc       Get Analytics
// @route      POST /api/v1/analytics
// @access     Protected && Restricted
export const getAnalytics = asyncHandler(async (req, res, next) => {
  let startTime: Moment, endTime: Moment;
  let period = req.body.period || (req.body.period === '' && 'day') || 'day';
  let date = req.body.date || new Date();
  let document = req.body.document || 'laundry';
  let result: ILaundry[] | IUser[] | ICustomer[] = [];

  startTime = moment(date).startOf(period);
  endTime = moment(date).endOf(period);

  switch (document) {
    case 'laundry':
      result = await Laundry.find({
        createdAt: { $gte: startTime, $lt: endTime },
      }).sort({ createdAt: 'asc' });
      break;

    case 'user':
      result = await User.find({
        createdAt: { $gte: startTime, $lt: endTime },
      }).sort({ createdAt: 'asc' });
      break;

    case 'customer':
      result = await Customer.find({
        createdAt: { $gte: startTime, $lt: endTime },
      }).sort({ createdAt: 'asc' });
      break;

    default:
      break;
  }

  const data = {
    period,
    date,
    startTime: startTime.format('ddd, MMM Do YY, ha'),
    endTime: endTime.format('ddd, MMM Do YY, ha'),
    document,
    count: result.length,
    result,
  };

  res.status(200).json({
    success: 'true',
    data,
  });
});

// @desc       Get Sales Analytics
// @route      POST /api/v1/analytics/sales
// @access     Protected && Restricted
export const getSalesAnalytics = asyncHandler(async (req, res, next) => {
  if (!(req.query.type === 'sales' || req.query.type === 'laundry')) {
    res.status(401);
    throw new Error('Invalid query');
  }

  let startTime: Moment,
    endTime: Moment,
    prevStartTime: Moment,
    prevEndTime: Moment;

  let period =
    req.body.period || (req.body.period === '' && 'month') || 'month';
  let date = req.body.date || new Date();
  let prevDate = moment(date).subtract(1, period).add(1, 'hour');

  startTime = moment(date).startOf(period);
  endTime = moment(date).endOf(period);
  prevStartTime = moment(prevDate).startOf(period);
  prevEndTime = moment(prevDate).endOf(period);

  if (req.query.type === 'sales') {
    let salesStats: {
      oldSales: number;
      newSales: number;
      percentageChange: number;
    } = {
      oldSales: 0,
      newSales: 0,
      percentageChange: 0,
    };

    const prevSales = await Laundry.find({
      progress: 'delivered',
      createdAt: { $gte: prevStartTime, $lt: prevEndTime },
    }).select(['total', 'createdAt']);

    const newSales = await Laundry.find({
      progress: 'delivered',
      createdAt: { $gte: startTime, $lt: endTime },
    }).select(['total', 'createdAt']);

    const totalPrevSalesArr =
      prevSales.length === 0 ? [] : prevSales.map((sales) => sales.total);
    const totalNewSalesArr =
      newSales.length === 0 ? [] : newSales.map((sales) => sales.total);

    const totalPrevSales =
      totalPrevSalesArr.length === 0
        ? 0
        : totalPrevSalesArr.reduce((acc, curr) => acc + curr);
    const totalNewSales =
      totalNewSalesArr.length === 0
        ? 0
        : totalNewSalesArr.reduce((acc, curr) => acc + curr);

    const percentageChange =
      ((totalNewSales - totalPrevSales) / totalPrevSales) * 100;

    salesStats.oldSales = totalPrevSales;
    salesStats.newSales = totalNewSales;
    salesStats.percentageChange = percentageChange;

    res.status(200).json({
      success: true,
      data: {
        date,
        period,
        prevDate,
        salesStats,
      },
    });
  }

  if (req.query.type === 'laundry') {
    const laundry = await Laundry.find({
      progress: 'delivered',
      createdAt: { $gte: startTime, $lt: endTime },
    }).select(['total', 'createdAt']);

    if (req.body.period === 'year' || !req.body.period) {
      let monthlyData: { month: number; total: number }[] = [];
      let stats: { month: string; total: number }[] = [];

      if (laundry.length > 0) {
        laundry.forEach((laundry) => {
          monthlyData = [
            ...monthlyData,
            {
              month: moment(laundry.createdAt).month(),
              total: laundry.total,
            },
          ];
        });
      }

      if (monthlyData.length > 0) {
        for (let i = 0; i < 12; i++) {
          stats = [
            ...stats,
            {
              month: UtilityService.months[i],
              total: monthlyData
                .map((data) => (data.month === i ? data.total : 0))
                .reduce((acc, curr) => acc + curr),
            },
          ];
        }
      }

      res.status(200).json({
        success: true,
        date,
        period,
        stats,
      });
    }

    if (req.body.period === 'month') {
      let weeklyData: { week: number; total: number }[] = [];
      let stats: { week: string; total: number }[] = [];

      if (laundry.length > 0) {
        laundry.forEach((laundry) => {
          weeklyData = [
            ...weeklyData,
            {
              week: Math.ceil(moment(laundry.createdAt).date() / 7),
              total: laundry.total,
            },
          ];
        });
      }

      if (weeklyData.length > 0) {
        for (let i = 0; i < 4; i++) {
          stats = [
            ...stats,
            {
              week: UtilityService.weeks[i],
              total: weeklyData
                .map((data) => (data.week === i ? data.total : 0))
                .reduce((acc, curr) => acc + curr),
            },
          ];
        }
      }

      res.status(200).json({
        success: true,
        date,
        period,
        stats,
      });
    }
  }
});
