import asyncHandler from 'express-async-handler'
import {
  createOne,
  deleteOne,
  getAll,
  getOne,
  updateOne,
} from '../middleware/query'
import Customer from '../models/Customer'
import Laundry from '../models/Laundry'
import { Role } from '../models/User'
import APIFeatures from '../utils/apiFeatures'

// @desc       Create Laundry
// @route      POST /api/v1/laundry
// @access     Protected
export const createLaundry = asyncHandler(async (req, res, next) => {
  const laundry = await Laundry.create(req.body)

  const customer = await Customer.findOne({ email: laundry.user.email })

  if (!customer) {
    await Customer.create({
      name: laundry.user.name,
      email: laundry.user.email,
      phone: laundry.user.phone,
      address: laundry.user.address,
      points: 0.1,
    })
  } else {
    await Customer.findByIdAndUpdate(
      customer._id,
      { points: customer.points + 0.1 },
      {
        new: true,
        runValidators: true,
      },
    )
  }

  res.status(201).json({
    success: true,
    data: laundry,
  })
})

// @desc       Get All Laundries
// @route      GET /api/v1/laundry
// @access     Protected
export const getLaundries = asyncHandler(async (req, res, next) => {
  if (req.user.role === Role.user) {
    const laundry = await Laundry.find({ email: req.user.email })

    res.status(200).json({
      success: true,
      data: laundry,
    })
  }

  if (req.user.role !== Role.user) {
    let reqQuery = { ...req.query }

    const excludedFields = ['select', 'sort', 'page', 'limit']
    excludedFields.forEach(param => delete reqQuery[param])

    // Advance filtering
    let queryStr = JSON.stringify(reqQuery)
    queryStr = queryStr.replace(/\b(gt|gte|lt|lte|in)\b/g, match => `$${match}`)

    let filter = {}

    const features = new APIFeatures(Laundry.find(filter), req.query)
      .filter()
      .sort()
      .paginate()
      .selectFields()

    const laundry = await features.query
    const totalCount = await (await Laundry.find(JSON.parse(queryStr))).length

    res.status(200).json({
      success: true,
      totalCount,
      count: laundry.length,
      data: laundry,
    })
  }
})

// @desc       Get A Laundry
// @route      GET /api/v1/laundry/:id
// @access     Protected
export const getLaundry = getOne(Laundry)

// @desc       Update A Laundry
// @route      PATCH /api/v1/laundry/:id
// @access     Protected
export const updateLaundry = updateOne(Laundry)

// @desc       Delete A Laundry
// @route      DELETE /api/v1/laundry/:id
// @access     Protected
export const deleteLaundry = deleteOne(Laundry)
