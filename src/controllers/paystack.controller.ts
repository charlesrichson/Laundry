import { createOne, getAll, getOne } from '../middleware/query'
import Paystack from '../models/Paystack'

// @desc       Create Payment
// @route      POST /api/v1/paystack
// @access     Protected
export const createPayment = createOne(Paystack)

// @desc       Get Payments
// @route      GET /api/v1/paystack
// @access     Protected
export const getPayments = getAll(Paystack)

// @desc       Get Payment
// @route      GET /api/v1/paystack/:id
// @access     Protected
export const getPayment = getOne(Paystack)
