import asyncHandler from 'express-async-handler'
import { sendTokenResponse } from '../middleware/auth'
import User from '../models/User'
import UtilityService from '../utils/utils'

// @desc       Signup
// @route      POST /api/v1/auth/signup
// @access     Public
export const signup = asyncHandler(async (req, res, next) => {
  const { name, email, password, passwordConfirm } = req.body

  const user = await User.find({ email })

  if (user.length > 0) {
    res.status(400)
    throw new Error('User already exists.')
  }

  if (password !== passwordConfirm) {
    res.status(400)
    throw new Error('Passwords do not match.')
  }

  const newUser = await User.create({
    name,
    password,
    passwordConfirm,
    email,
  })

  newUser.phoneVerifyToken = UtilityService.randomDigits()

  await newUser.save()

  sendTokenResponse(newUser._id, 201, res)
})

// @desc       Login
// @route      POST /api/v1/auth/signup
// @access     Public
export const login = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body

  if (!email || !password) {
    res.status(400)
    throw new Error('Please provide email and password.')
  }

  const user = await User.findOne({ email }).select('+password')

  if (!user || !(await user.verifyPassword(password, user.password))) {
    res.status(400)
    throw new Error('Invalid Credentials')
  }

  sendTokenResponse(user._id, 200, res)
})

// @desc       Forgot password
// @route      POST /api/v1/auth/forgotPassword
// @access     Public
export const forgotPassword = asyncHandler(async (req, res, next) => {
  if (!req.body.email) {
    res.status(400)
    throw new Error('Please enter your email address.')
  }

  const user = await User.findOne({ email: req.body.email })

  if (!user) {
    res.status(404)
    throw new Error('No user with this email address.')
  }

  const resetToken = user.createResetToken()
  await user.save({ validateBeforeSave: false })

  const message = `Forgot your password? Click on this link http://localhost:3000/auth?type=reset&resetToken=${resetToken}\nIf you didn't forget your password, please ignore this email.`

  // TODO Change message

  try {
    await UtilityService.sendMail({
      email: req.body.email,
      subject: 'Your password reset token. (Valid for 10 mins)',
      message,
    })

    res
      .status(200)
      .json({ success: true, data: 'Reset Token sent, check your email.' })
  } catch (err) {
    user.passwordResetToken = undefined
    user.passwordResetTokenExpires = undefined
    await user.save({ validateBeforeSave: false })

    throw new Error('An error occured. Try again later.')
  }
})

// @desc       Reset password
// @route      POST /api/v1/auth/resetPassword/:token
// @access     Public
export const resetPassword = asyncHandler(async (req, res, next) => {
  const hashedToken = UtilityService.createHashToken(req.params.token)

  const user = await User.findOne({
    passwordResetToken: hashedToken,
    passwordResetTokenExpires: { $gt: Date.now() },
  })

  if (!user) {
    res.status(400)
    throw new Error('Invalid or expired Token. Please try again')
  }

  user.password = req.body.password
  user.passwordResetToken = undefined
  user.passwordResetTokenExpires = undefined
  await user.save()

  sendTokenResponse(user._id, 200, res)
})

// @desc       Update password
// @route      POST /api/v1/auth/updatePassword
// @access     Protected
export const updatePassword = asyncHandler(async (req, res, next) => {
  if (
    !req.body.currentPassword ||
    !req.body.password ||
    !req.body.passwordConfirm
  ) {
    res.status(400)
    throw new Error('Please fill all fields')
  }

  if (req.body.password !== req.body.passwordConfirm) {
    res.status(400)
    throw new Error('Passwords do not match.')
  }

  const user = await User.findById(req.user.id).select('+password')

  if (user) {
    if (!(await user.verifyPassword(req.body.currentPassword, user.password))) {
      res.status(400)
      throw new Error('Your current passoword is wrong')
    }

    user.password = req.body.password
    await user.save()

    sendTokenResponse(user._id, 200, res)
  }
})

// @desc       Verify User's Phone
// @route      POST /api/v1/auth/verify/phone
// @access     Private
export const verifyPhone = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id)

  if (!user) {
    res.status(404)
    throw new Error('User not found')
  }

  if (!req.body.phoneToken) {
    if (!user.phone) {
      res.status(400)
      throw new Error('Phone number not found')
    }

    await UtilityService.sendText({
      phone: user.phone,
      message: `Verification code: ${user.phoneVerifyToken}. Valid for 5 minutes.`,
    })

    res.status(200).json({
      success: true,
      data: 'Verfication code has been sent to your device.',
    })
  }

  if (req.body.phoneToken) {
    const isMatch = user.phoneVerifyToken === req.body.phoneToken

    if (!isMatch) {
      res.status(400)
      throw new Error('Invalid verification code')
    }

    user.phoneVerified = true

    await user.save()

    res.status(200).json({
      success: true,
      data: 'Phone number verified',
    })
  }
})

// @desc       Verify User's Email
// @route      POST /api/v1/auth/verify/email
// @access     Private
export const verifyEmail = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id)

  if (!user) {
    res.status(404)
    throw new Error('User not found')
  }

  if (!req.body.verified) {
    await UtilityService.sendMail({
      email: user.email,
      subject: `Hi ${user.name}, please verify your Instant Wash N Dry Laundromat account`,
      message: 'Click on this link to verify your email http...',
      // TODO Attach frontend link
    })

    res.status(200).json({
      success: true,
      data: 'Email verification link sent to email.',
    })
  }

  user.verified = true
  await user.save()

  res.status(200).json({
    success: true,
    data: 'Email verified.',
  })
})
