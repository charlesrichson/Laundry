import { createOne, updateOne, getAll } from '../middleware/query'
import Prices from '../models/Prices'

// @desc       Create Prices
// @route      POST /api/v1/prices
// @access     Protected && Restricted
export const createPrices = createOne(Prices)

// @desc       Get Prices
// @route      GET /api/v1/prices
// @access     Protected
export const getPrices = getAll(Prices)

// @desc       Update Prices
// @route      PATCH /api/v1/prices/:id
// @access     Protected && Restricted
export const updatePrices = updateOne(Prices)
