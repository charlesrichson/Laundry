import asyncHandler from 'express-async-handler'
import { Response, Request } from 'express'
import {
  deleteOne,
  getAll,
  getOne,
  updateOne,
  search,
} from '../middleware/query'
import Customer from '../models/Customer'

// @desc       Create Customer
// @route      POST /api/v1/customer
// @access     Private & Restricted
export const createCustomer = asyncHandler(async (req, res, next) => {
  if (!req.body.name || !req.body.email || !req.body.phone) {
    res.status(400)
    throw new Error('Incomplete details')
  }

  const customer = await Customer.findOne({ email: req.body.email })

  if (customer) {
    res.status(400)
    throw new Error('Customer already exists')
  }

  const newCustomer = await Customer.create(req.body)

  res.status(201).json({
    success: true,
    data: newCustomer,
  })
})

// @desc       Get Customers
// @route      GET /api/v1/customer
// @access     Private & Restricted
export const getCustomers = getAll(Customer)

// @desc       Get Customers
// @route      GET /api/v1/customer/:id
// @access     Private & Restricted
export const getCustomer = getOne(Customer)

// @desc       Update Customer
// @route      PATCH /api/v1/customer/
// @access     Private & Restricted
export const updateCustomer = updateOne(Customer)

// @desc       Delete Customer
// @route      DELETE /api/v1/customer/:id
// @access     Private & Restricted
export const deleteCustomer = deleteOne(Customer)

// @desc       Search Customer
// @route      GET /api/v1/customer/search/:query
// @access     Private & Restricted
export const searchCustomer = search(Customer, 'name')
