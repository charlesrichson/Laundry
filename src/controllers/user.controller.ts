import asyncHandler from 'express-async-handler'
import User, { Role } from '../models/User'
import _ from 'lodash'
import { getAll, getOne } from '../middleware/query'
import Laundry from '../models/Laundry'

// @desc       Update user
// @route      PATCH /api/v1/user/updateUser
// @access     Protected
export const updateUser = asyncHandler(async (req, res, next) => {
  if (req.body.password || req.body.passwordConfirm) {
    res.status(400)
    throw new Error('Unable to update password via this endpoint')
  }

  if (req.body.userId && req.user.role === Role.user) {
    res.status(401)
    throw new Error('Unauthorized to perform this action')
  }

  if (!req.body.userId) {
    const filteredBody = _.omit(req.body, 'role')

    if (req.file) filteredBody.photo = req.file.filename

    const updatedUser = await User.findByIdAndUpdate(
      req.user.id,
      filteredBody,
      {
        new: true,
        runValidators: true,
      },
    )

    if (!updatedUser) {
      res.status(404)
      throw new Error('User not found')
    }

    res.status(200).json({
      success: true,
      data: updatedUser,
    })
  }

  if (req.body.userId && req.user.role === Role.manager) {
    const { role } = req.body
    const updatedUser = await User.findByIdAndUpdate(
      req.body.userId,
      { role },
      {
        new: true,
        runValidators: true,
      },
    )

    if (!updatedUser) {
      res.status(404)
      throw new Error('User not found')
    }

    res.status(200).json({
      success: true,
      data: updatedUser,
    })
  }
})

// @desc       Delete User
// @route      DELETE /api/v1/user/:id
// @access     Protected
export const deleteUser = asyncHandler(async (req, res, next) => {
  if (req.user.role === Role.user) {
    await User.findByIdAndDelete(req.user.id)

    await Laundry.deleteMany({ user: req.user.id })

    res.status(204).json({
      success: true,
      data: null,
    })
  }

  if (req.user.role === Role.manager) {
    await User.findByIdAndDelete(req.body.userId)

    await Laundry.deleteMany({ user: req.body.userId })

    res.status(204).json({
      success: true,
      data: null,
    })
  }
})

// @desc       Get User By ID || Get Me
// @route      GET /api/v1/user
// @access     Protected
export const getUser = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user.id)

  if (!user) {
    res.status(404)
    throw new Error('User not found.')
  }

  res.status(200).json({
    success: true,
    data: user,
  })
})

export const getUserById = getOne(User)

// @desc       Get Users
// @route      GET /api/v1/user/all
// @access     Protected && Restricted
export const getUsers = getAll(User)
