import { Types } from 'mongoose'
import asyncHandler from 'express-async-handler'
import ExpoToken from '../models/ExpoToken'
import Notification from '../models/Notification'
import UtilityService from '../utils/utils'

// @desc       Get notifications
// @route      GET /api/v1/notification
// @access     Private
export const getNotifications = asyncHandler(async (req, res, next) => {
  const notifications = await Notification.find({ user: req.user.id })

  res.status(200).json({
    success: true,
    data: notifications,
  })
})

// @desc       Create notifications
// @route      POST /api/v1/notification
// @access     Private && Restricted
export const createNotification = asyncHandler(async (req, res, next) => {
  let data: { user: string; message: string }[] = []

  if (!req.body.users || req.body.users.length === 0 || !req.body.message) {
    res.status(400)
    throw new Error('No user or message')
  }

  for (let i = 0; i < req.body.users.length; i++) {
    data.push({ user: req.body.users[i]._id, message: req.body.message })
  }

  const users = await (
    await Notification.create(data)
  ).map(notification => Types.ObjectId(`${notification.user}`))

  const tokens = await (
    await ExpoToken.find({
      user: {
        $in: [...users],
      },
    })
  )
    .filter(token => token.isSubscribed)
    .map(token => token.pushToken)

  UtilityService.sendNotification({
    pushTokens: [...tokens],
    message: req.body.message,
  })

  res.status(201).json({
    success: true,
    data: 'Push notification sent for processing',
  })
})

// @desc       Delete all notifications
// @route      DELETE /api/v1/notification
// @access     Private && Restricted
export const deleteNotifications = asyncHandler(async (req, res, next) => {
  await Notification.deleteMany()

  res.status(204).json({
    success: true,
    data: null,
  })
})
