import asyncHandler from 'express-async-handler'
import { createOne, deleteOne, getOne, updateOne } from '../middleware/query'
import Cloth from '../models/Cloth'

// @desc       Create Cloth
// @route      POST /api/v1/cloth
// @access     Private && Restricted
export const createCloth = createOne(Cloth)

// @desc       Get Clothes
// @route      GET /api/v1/cloth
// @access     Private && Restricted
export const getClothes = asyncHandler(async (req, res, next) => {
  const clothes = await Cloth.find()

  res.status(200).json({
    success: true,
    data: clothes,
  })
})

// @desc       Get Cloth
// @route      GET /api/v1/cloth/:id
// @access     Private && Restricted
export const getCloth = getOne(Cloth)

// @desc       Delete Cloth
// @route      DELETE /api/v1/cloth/:id
// @access     Private && Restricted
export const deleteCloth = deleteOne(Cloth)

// @desc       Update Cloth
// @route      PATCH /api/v1/cloth/:id
// @access     Private && Restricted
export const updateCloth = updateOne(Cloth)
