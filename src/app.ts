import express, { Application } from 'express'
import ExpressMongoSanitize from 'express-mongo-sanitize'
import helmet, { xssFilter } from 'helmet'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import connectDB from './config/db'
import { errorHandler, notFound } from './middleware/error'
import config from './config/config'

// Connect to DB
connectDB()

// Router Files
import auth from './routes/auth.routes'
import user from './routes/user.routes'
import laundry from './routes/laundry.routes'
import prices from './routes/prices.routes'
import settings from './routes/settings.routes'
import attendance from './routes/attendance.routes'
import expoToken from './routes/expoToken.routes'
import notification from './routes/notification.routes'
import message from './routes/message.routes'
import paystack from './routes/paystack.routes'
import customer from './routes/customer.routes'
import cloth from './routes/cloth.routes'
import analytics from './routes/analytics.routes'

const app: Application = express()

// Render static files
app.use(express.static('public/docs'))

// Secure HTTP Headers
app.use(helmet())

// Body Parser
app.use(express.json())

// Data Sanitization
app.use(ExpressMongoSanitize())

//Cookie Parser
app.use(cookieParser())

// Enable CORS
app.use(cors())

// Prevent XSS Attacks
app.use(xssFilter())

// Dev logging middleware
if (config.node_env === 'development') app.use(morgan('dev'))

// Mount Routers
app.use('/api/v1/auth', auth)
app.use('/api/v1/user', user)
app.use('/api/v1/laundry', laundry)
app.use('/api/v1/prices', prices)
app.use('/api/v1/settings', settings)
app.use('/api/v1/attendance', attendance)
app.use('/api/v1/token', expoToken)
app.use('/api/v1/notification', notification)
app.use('/api/v1/message', message)
app.use('/api/v1/paystack', paystack)
app.use('/api/v1/customer', customer)
app.use('/api/v1/cloth', cloth)
app.use('/api/v1/analytics', analytics)

app.use(notFound)

app.use(errorHandler)

export default app
