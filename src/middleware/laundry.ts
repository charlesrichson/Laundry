import { NextFunction, Request, Response } from 'express'
import { Role } from '../models/User'
import _ from 'lodash'

export const allowUpdate = (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  if (req.user.role !== Role.user) return next()

  if (req.user.role === Role.user) {
    const excludedFields = [
      'progress',
      'pickedAt',
      'isPicked',
      'deliveredAt',
      'isDelivered',
    ]
    excludedFields.forEach(param => delete req.body[param])

    next()
  }
}
