import asyncHandler from 'express-async-handler'
import { Model } from 'mongoose'
import APIFeatures from '../utils/apiFeatures'

export const deleteOne = (Model: Model<any>) =>
  asyncHandler(async (req, res, next) => {
    const doc = await Model.findByIdAndDelete(req.params.id)

    if (!doc) {
      res.status(404)
      throw new Error('Document not found')
    }

    res.status(204).json({
      success: true,
      data: null,
    })
  })

export const getOne = (Model: Model<any>, populate?: string) =>
  asyncHandler(async (req, res, next) => {
    let query = Model.findById(req.params.id).select('+active')
    if (populate) query = query.populate(populate)

    const doc = await query

    if (!doc) {
      res.status(404)
      throw new Error('Document not found')
    }

    res.status(200).json({ success: true, data: doc })
  })

export const createOne = (Model: Model<any>) =>
  asyncHandler(async (req, res, next) => {
    const doc = await Model.create(req.body)

    res.status(201).json({
      success: true,
      data: doc,
    })
  })

export const updateOne = (Model: Model<any>) =>
  asyncHandler(async (req, res, next) => {
    const doc = await Model.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    })

    if (!doc) {
      res.status(404)
      throw new Error('Document not found')
    }

    res.status(200).json({
      success: true,
      data: doc,
    })
  })

export const getAll = (Model: Model<any>) =>
  asyncHandler(async (req, res, next) => {
    let reqQuery = { ...req.query }

    const excludedFields = ['select', 'sort', 'page', 'limit']
    excludedFields.forEach(param => delete reqQuery[param])

    // Advance filtering
    let queryStr = JSON.stringify(reqQuery)
    queryStr = queryStr.replace(/\b(gt|gte|lt|lte|in)\b/g, match => `$${match}`)

    let filter = {}

    const features = new APIFeatures(Model.find(filter), req.query)
      .filter()
      .sort()
      .paginate()
      .selectFields()

    const doc = await features.query
    const totalCount = await (await Model.find(JSON.parse(queryStr))).length

    res.status(200).json({
      success: true,
      totalCount,
      count: doc.length,
      data: doc,
    })
  })

export const search = (Model: Model<any>, field: string) =>
  asyncHandler(async (req, res, next) => {
    let reqQuery = { ...req.query }

    const excludedFields = ['select', 'sort', 'page', 'limit']
    excludedFields.forEach(param => delete reqQuery[param])

    // Advance filtering
    let queryStr = JSON.stringify(reqQuery)
    queryStr = queryStr.replace(/\b(gt|gte|lt|lte|in)\b/g, match => `$${match}`)

    const regex = new RegExp(req.params.query, 'i')

    let filter: any

    if (field === 'name') filter = { $and: [{ $or: [{ name: regex }] }] }

    const features = new APIFeatures(Model.find(filter), req.query)
      .filter()
      .sort()
      .paginate()
      .selectFields()

    const doc = await features.query
    const totalCount = await (await Model.find(filter)).length

    res.status(200).json({
      success: true,
      totalCount,
      count: doc.length,
      data: doc,
    })
  })
