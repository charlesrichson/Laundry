import asyncHandler from 'express-async-handler'
import { NextFunction, Request, Response } from 'express'
import UtilityService from '../utils/utils'
import User, { Role } from '../models/User'
import { ITokenPayload } from '../utils/interface'
import config from '../config/config'

export const sendTokenResponse = (
  id: string,
  statusCode: number,
  res: Response,
) => {
  const token = UtilityService.signToken(id)

  const cookieExpiration = new Date(
    Date.now() + config.jwt.cookie_expires_in * 24 * 60 * 60 * 1000,
  )

  const cookieOptions = {
    expires: cookieExpiration,
    httpOnly: true,
    secure: false,
  }

  if (config.node_env === 'production') cookieOptions.secure === true

  res.status(statusCode).cookie('jwt', token, cookieOptions).json({
    success: true,
    token,
  })
}

export const protect = asyncHandler(async (req, res, next) => {
  let token

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1]
  } else if (req.cookies.token) token = req.cookies.token

  if (!token) {
    res.status(400)
    throw new Error('Unauthorized Please Login')
  }

  const decoded = UtilityService.verifyToken(token)

  const { id, iat } = decoded as ITokenPayload

  const user = await User.findById(id)

  if (!user) {
    res.status(404)
    throw new Error('User not found')
  }

  if (user.changedPasswordAfter(iat)) {
    res.status(401)
    throw new Error('Password recently changed. Please login')
  }

  req.user = user

  next()
})

export const restrictTo =
  (...roles: string[]) =>
  (req: Request, res: Response, next: NextFunction) => {
    if (!roles.includes(req.user.role)) {
      res.status(403)
      throw new Error('Permission denied to perform this action')
    }

    next()
  }
