import { updatePrices } from './../controllers/prices.controller'
import express from 'express'
import { getPrices, createPrices } from '../controllers/prices.controller'
import { protect, restrictTo } from '../middleware/auth'
import { Role } from '../models/User'

const router = express.Router()

router.route('/').get(getPrices).post(protect, restrictTo(Role.manager), createPrices)

router.patch('/:id',protect, restrictTo(Role.manager), updatePrices)

export default router
