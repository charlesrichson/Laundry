import { verifyPhone, verifyEmail } from './../controllers/auth.controller'
import express from 'express'
import {
  forgotPassword,
  login,
  resetPassword,
  signup,
  updatePassword,
} from '../controllers/auth.controller'
import { protect } from '../middleware/auth'

const router = express.Router()

router.post('/login', login)
router.post('/signup', signup)

router.post('/forgotPassword', forgotPassword)
router.patch('/resetPassword/:token', resetPassword)
router.patch('/updatePassword', protect, updatePassword)

router.post('/verify/phone', protect, verifyPhone)
router.post('/verify/email', protect, verifyEmail)

export default router
