import { protect, restrictTo } from './../middleware/auth';
import { Router } from 'express';
import {
  getAnalytics,
  getSalesAnalytics,
} from '../controllers/analytics.controller';

const router = Router();

router.use(protect);

router.post('/', restrictTo('attendant', 'manager'), getAnalytics);

router.post('/sales', restrictTo('attendant', 'manager'), getSalesAnalytics);

export default router;
