import {
  createCustomer,
  deleteCustomer,
  getCustomer,
  getCustomers,
  searchCustomer,
  updateCustomer,
} from './../controllers/customer.controller'
import { protect, restrictTo } from './../middleware/auth'
import { Router } from 'express'

const router = Router()

router.use(protect)
router.use(restrictTo('attendant', 'manager', 'dispatch'))

router.route('/').get(getCustomers).post(createCustomer)

router.get('/search/:query', searchCustomer)

router
  .route('/:id')
  .get(getCustomer)
  .patch(updateCustomer)
  .delete(deleteCustomer)

export default router
