import {
  createMessage,
  deleteMessages,
  getMessages,
  getMessage,
} from './../controllers/message.controller'
import { protect, restrictTo } from './../middleware/auth'
import { Router } from 'express'
import { Role } from '../models/User'

const router = Router()

router
  .route('/')
  .get(protect, restrictTo(Role.manager, Role.attendant), getMessages)
  .post(createMessage)
  .delete(protect, restrictTo(Role.manager), deleteMessages)

router.get(
  '/:id',
  protect,
  restrictTo(Role.manager, Role.attendant),
  getMessage,
)

export default router
