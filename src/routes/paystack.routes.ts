import { protect } from './../middleware/auth'
import express from 'express'
import {
  createPayment,
  getPayment,
  getPayments,
} from '../controllers/paystack.controller'

const router = express.Router()

router.use(protect)

router.route('/').get(getPayments).post(createPayment)

router.get('/:id', getPayment)

export default router
