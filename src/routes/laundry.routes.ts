import express from 'express'
import {
  createLaundry,
  deleteLaundry,
  getLaundries,
  getLaundry,
  updateLaundry,
} from '../controllers/laundry.controller'
import { protect, restrictTo } from '../middleware/auth'
import { allowUpdate } from '../middleware/laundry'
import { Role } from '../models/User'

const router = express.Router()

router.use(protect)

router.route('/').post(createLaundry).get(getLaundries)

router
  .route('/:id')
  .get(getLaundry)
  .patch(allowUpdate, updateLaundry)
  .delete(restrictTo(Role.manager), deleteLaundry)

export default router
