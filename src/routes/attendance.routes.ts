import {
  deleteAttendance,
  getAttendance,
  createAttendance,
  updateAttendance,
} from '../controllers/attendance.controller'
import express from 'express'
import { protect, restrictTo } from '../middleware/auth'
import { Role } from '../models/User'

const router = express.Router()

router
  .route('/')
  .get(protect, restrictTo(Role.manager), getAttendance)
  .delete(protect, restrictTo(Role.manager), deleteAttendance)
  .post(createAttendance)

router.patch('/:employee', updateAttendance)

export default router
