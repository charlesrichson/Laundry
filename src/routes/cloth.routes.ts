import {
  getClothes,
  createCloth,
  getCloth,
  updateCloth,
  deleteCloth,
} from './../controllers/cloth.controller'
import { protect, restrictTo } from './../middleware/auth'
import express from 'express'
import { Role } from '../models/User'

const router = express.Router()

router.use(protect)
router.use(restrictTo(Role.attendant, Role.dispatch, Role.manager))

router.route('/').get(getClothes).post(createCloth)

router.route('/:id').get(getCloth).patch(updateCloth).delete(deleteCloth)

export default router
