import {
  updateSettings,
  createSettings,
} from './../controllers/settings.controller'
import express from 'express'
import { getSettings } from '../controllers/settings.controller'
import { protect, restrictTo } from '../middleware/auth'
import { Role } from '../models/User'

const router = express.Router()

router
  .route('/')
  .get(getSettings)
  .post(protect, restrictTo(Role.manager), createSettings)

router.route('/:id').patch(protect, restrictTo(Role.manager), updateSettings)

export default router
