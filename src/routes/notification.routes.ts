import {
  getNotifications,
  createNotification,
  deleteNotifications,
} from '../controllers/notification.controller'
import express from 'express'
import { protect, restrictTo } from '../middleware/auth'
import { Role } from '../models/User'

const router = express.Router()

router.use(protect)

router
  .route('/')
  .get(restrictTo(Role.user), getNotifications)
  .post(
    restrictTo(Role.attendant, Role.dispatch, Role.manager),
    createNotification,
  )
  .delete(restrictTo(Role.manager), deleteNotifications)

export default router
