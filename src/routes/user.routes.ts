import express from 'express'
import {
  updateUser,
  deleteUser,
  getUser,
  getUsers,
  getUserById,
} from '../controllers/user.controller'
import { protect, restrictTo } from '../middleware/auth'
import { uploadPhoto } from '../middleware/image'
import { Role } from '../models/User'

const router = express.Router()

router.use(protect)

router.route('/').get(getUser).delete(deleteUser)
router.patch('/updateUser', uploadPhoto, updateUser)
router.get(
  '/all',
  restrictTo(Role.manager, Role.attendant, Role.dispatch),
  getUsers,
)
router.get(
  '/all/:id',
  restrictTo(Role.manager, Role.attendant, Role.dispatch),
  getUserById,
)

export default router
