import {
  getTokens,
  createToken,
  getToken,
  updateToken,
  deleteToken,
} from './../controllers/expoToken.controller'
import express from 'express'
import { protect, restrictTo } from '../middleware/auth'
import { Role } from '../models/User'

const router = express.Router()

router.use(protect)

router
  .route('/')
  .get(restrictTo(Role.attendant, Role.dispatch, Role.manager), getTokens)
  .post(createToken)

router.route('/:id').get(getToken).patch(updateToken).delete(deleteToken)

export default router
