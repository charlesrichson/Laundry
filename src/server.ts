import app from './app'
import config from './config/config'

const port = process.env.PORT || config.port || 5000

const server = app.listen(port, () =>
  console.log(`Server running on port ${port} ✅`),
)

// Handle unhandled promise rejections
process.on('unhandledRejection', (err: any, promise) => {
  console.log(`Error: ${err.message} ⛔`)

  // Close server and exit process
  server.close(() => process.exit(1))
})
