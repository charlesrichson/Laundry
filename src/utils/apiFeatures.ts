class APIFeatures {
  query: any
  queryString: any

  constructor(query: any, queryString: any) {
    this.query = query
    this.queryString = queryString
  }

  filter() {
    let reqQuery = { ...this.queryString }

    const excludedFields = ['select', 'sort', 'page', 'limit']
    excludedFields.forEach(param => delete reqQuery[param])

    // Advance filtering
    let queryStr = JSON.stringify(reqQuery)
    queryStr = queryStr.replace(/\b(gt|gte|lt|lte|in)\b/g, match => `$${match}`)

    this.query = this.query.find(JSON.parse(queryStr))
    return this
  }

  selectFields() {
    if (this.queryString.select) {
      const fields = this.queryString.select.split(',').join(' ')
      this.query = this.query.select(fields)
    } else this.query = this.query.select('-__v')

    return this
  }

  sort() {
    if (this.queryString.sort) {
      const sortBy = this.queryString.sort.split(',').join(' ')
      this.query = this.query.sort(sortBy)
    } else {
      this.query = this.query.sort('-createdAt')
    }

    return this
  }

  paginate() {
    const page = parseInt(this.queryString.page, 10) || 1
    const limit = parseInt(this.queryString.limit, 10) || 20
    const skip = (page - 1) * limit

    this.query = this.query.skip(skip).limit(limit)

    return this
  }

  populate(populate: string) {
    this.query = this.query.populate(populate)
    return this
  }
}

export default APIFeatures
