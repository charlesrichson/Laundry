import { readFile, writeFile } from 'fs';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import config from '../config/config';
import {
  IMailOptions,
  IPushNotificationsOptions,
  ITextOptions,
} from './interface';
import mailgun from 'mailgun-js';
import Vonage from '@vonage/server-sdk';
import Expo, { ExpoPushMessage, ExpoPushTicket } from 'expo-server-sdk';
import lo_ from 'lodash';

class Utils {
  constructor(
    public readonly oldPricesPath: string = 'src/files/oldPrices.json',
    public readonly months: string[] = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ],
    public readonly weeks: string[] = ['Week 1', 'Week 2', 'Week 3', 'Week 4'],
  ) {}

  signToken(id: string) {
    const token = jwt.sign({ id }, `${config.jwt.secret}`, {
      expiresIn: config.jwt.expiration,
    });

    return token;
  }

  verifyToken(token: string) {
    const decoded = jwt.verify(token, `${config.jwt.secret}`);
    return decoded;
  }

  createHashToken(resetToken: string) {
    const hashedToken = crypto
      .createHash('sha256')
      .update(resetToken)
      .digest('hex');

    return hashedToken;
  }

  async sendMail(options: IMailOptions) {
    const { email, subject, message } = options;

    const mail = mailgun({
      apiKey: config.mailgun.apiKey,
      domain: config.mailgun.domain,
    });

    const data = {
      from: 'Instant Wash And Dry Laundromat <noreply@instantwashndry.com>',
      to: `${email}, laundry@instantwashndry.com`,
      subject: subject,
      text: message,
    };

    mail.messages().send(data, (error, body) => console.log(body));
  }

  async sendText(options: ITextOptions) {
    const { apiKey, apiSecret } = config.vonage;
    const { phone, message } = options;

    const vonage = new Vonage({
      apiKey,
      apiSecret,
    });

    const from = 'Laundry';
    const to = `234${phone}`;
    const text = message;

    vonage.message.sendSms(
      from,
      to,
      text,
      { from, to, text },
      (error, data) => {
        if (error) {
          console.log(error);
        } else if (data.messages[0]['status'] === '0') {
          console.log('Message sent successfully.');
        } else {
          console.log(
            `Message failed with error: ${data.messages[0]['error-text']}`,
          );
        }
      },
    );
  }

  async sendNotification(options: IPushNotificationsOptions) {
    const { message, pushTokens } = options;

    const expo = new Expo();

    let messages: ExpoPushMessage[] = [];

    for (let pushToken of pushTokens) {
      if (!Expo.isExpoPushToken(pushToken)) {
        console.error(`Push token ${pushToken} is not a valid Expo push token`);
        continue;
      }

      messages.push({
        to: pushToken,
        sound: 'default',
        body: message,
        data: { withSome: 'data' },
      });
    }

    const chunks = expo.chunkPushNotifications(messages);

    let tickets: ExpoPushTicket[] = [];

    (async () => {
      for (let chunk of chunks) {
        try {
          let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
          console.log(ticketChunk);
          tickets.push(...ticketChunk);
        } catch (error) {
          console.error(error);
        }
      }
    })();
  }

  randomDigits() {
    const digits = lo_.random(1100, 9980);

    return digits;
  }

  readFile(
    path: string,
    callback: (err: NodeJS.ErrnoException | null, data: string) => void,
  ) {
    readFile(path, 'utf8', (err, data) => {
      callback(err, data);
    });
  }

  writeFile(
    path: string,
    data: string,
    callback: (err: NodeJS.ErrnoException | null, success: boolean) => void,
  ) {
    writeFile(path, data, (err) => {
      if (err) return callback(err, false);

      callback(null, true);
    });
  }
}

const UtilityService = new Utils();

export default UtilityService;
