import { Document, Types } from 'mongoose';
import { Gender, Role } from '../models/User';

export interface IUser extends Document {
  name: string;
  email: string;
  phone: number;
  verified: boolean;
  phoneVerified: boolean;
  photo: string;
  role: Role;
  gender: Gender;
  expoTokenId: Types.ObjectId;
  phoneVerifyToken: number;
  active: boolean;
  address: string;
  password: string;
  passwordChangedAt: Date;
  passwordResetToken: string | undefined;
  passwordResetTokenExpires: number | undefined;
  verifyPassword(
    candidatePassword: string,
    userPassword: string,
  ): Promise<boolean>;
  createResetToken(): string;
  changedPasswordAfter(timestamp: number): boolean;
}

export interface ILaundry extends Document {
  service: string;
  serviceType: string;
  serviceQty: number;
  iron: boolean;
  detergent: boolean;
  detergentQty: number;
  laundryBag: boolean;
  laundryBagQty: number;
  laundryDetails: {
    fabric: string;
    count: number;
  }[];
  email: string;
  totalCount: number;
  user: {
    name: string;
    phone: number;
    photo: string;
    address: string;
    email: string;
  };
  extraCareOpt: { product: string; quantity: number }[];
  branchLocation: string;
  location: string;
  isPaid: boolean;
  paidAt: Date;
  paymentDetails: any;
  isDelivered: boolean;
  deliveredAt: Date;
  isPicked: boolean;
  pickedAt: Date;
  paymentMethod: string;
  progress: string;
  createdAt: any;
  subTotal: number;
  total: number;
}

export interface IPrices extends Document {
  washDrySelf: number;
  drySelf: number;
  washDryDropoff: number;
  dryDropOff: number;
  ironDropoff: number;
  iron: number;
  detergent: number;
  bleach: number;
  softner: number;
  stainRemover: number;
  delivery: number;
  laundryBag: number;
  ironing: {
    name: string;
    key: string;
    price: number;
  }[];
}

export interface INotification extends Document {
  user: Types.ObjectId;
  message: string;
  read: boolean;
}

export interface ICloth extends Document {
  cloth: string;
  key: string;
}

export interface IAttendance extends Document {
  employee: Types.ObjectId;
  date: Date[];
}

export interface ITokenPayload {
  id: string;
  iat: number;
  exp: number;
}

export interface IExpoToken {
  user: Types.ObjectId;
  pushToken: string;
  isSubscribed: boolean;
}

export interface IMailOptions {
  email: string;
  message: string;
  subject: string;
}

export interface ITextOptions {
  phone: number;
  message: string;
}

export interface IPushNotificationsOptions {
  pushTokens: string[];
  message: string;
}

export interface ISettings extends Document {
  branches: string[];
  attendant: Types.ObjectId[];
  dispatch: Types.ObjectId[];
  manager: Types.ObjectId[];
}

export interface IMessage extends Document {
  name: string;
  email: string;
  message: string;
}

export interface IPaystack extends Document {
  laundry: Types.ObjectId;
  message: string;
  reference: string;
  status: string;
  transaction: string;
}

export interface ICustomer extends Document {
  name: string;
  phone: number;
  address: string;
  email: string;
  points: number;
}
