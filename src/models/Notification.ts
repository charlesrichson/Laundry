import mongoose, { Schema, Model } from 'mongoose'
import { INotification } from '../utils/interface'

const NotificationSchema: Schema<INotification> = new mongoose.Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: [true, 'Please select a user'],
    },

    message: {
      type: String,
      required: [true, 'A message is required'],
    },

    read: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true },
)

const Notification: Model<INotification> = mongoose.model(
  'Notification',
  NotificationSchema,
)

export default Notification
