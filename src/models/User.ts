import mongoose, { Model, Schema } from 'mongoose'
import validator from 'validator'
import bcrypt from 'bcryptjs'
import crypto from 'crypto'
import UtilityService from '../utils/utils'
import { IUser } from '../utils/interface'

export enum Role {
  user = 'user',
  manager = 'manager',
  dispatch = 'dispatch',
  attendant = 'attendant',
}

export enum Gender {
  male = 'male',
  femail = 'female',
}

const UserSchema: Schema<IUser> = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Name is required'],
      trim: true,
    },

    email: {
      type: String,
      unique: true,
      required: [true, 'Please enter your email'],
      lowercase: true,
      validate: [validator.isEmail, 'Please enter a valid email'],
    },

    phone: Number,

    expoTokenId: {
      type: Schema.Types.ObjectId,
      ref: 'ExpoToken',
    },

    gender: {
      type: String,
      enum: Gender,
    },

    verified: {
      type: Boolean,
      default: false,
    },

    phoneVerified: {
      type: Boolean,
      default: false,
    },

    photo: {
      type: String,
      default:
        'https://res.cloudinary.com/dgl9gerlc/image/upload/v1623723873/instantLaundry/default_d7tgge.png',
    },

    role: {
      type: String,
      enum: Role,
      default: 'user',
    },

    active: {
      type: Boolean,
      default: true,
    },

    address: {
      type: String,
    },

    password: {
      type: String,
      required: [true, 'Please enter a password'],
      minlength: 8,
      select: false,
    },

    phoneVerifyToken: Number,

    passwordChangedAt: Date,

    passwordResetToken: String,

    passwordResetTokenExpires: Date,
  },
  {
    timestamps: true,
  },
)

UserSchema.pre<IUser>('save', async function (next) {
  if (!this.isModified('password')) return next()

  const salt = await bcrypt.genSalt(12)

  this.password = await bcrypt.hash(this.password, salt)

  next()
})

UserSchema.pre('save', function (next) {
  if (!this.isModified('password') || this.isNew) return next()

  this.passwordChangedAt = new Date(Date.now() - 1000)
  next()
})

UserSchema.methods.verifyPassword = async function (
  candidatePassword: string,
  userPassword: string,
) {
  return await bcrypt.compare(candidatePassword, userPassword)
}

UserSchema.methods.changedPasswordAfter = function (JWTTimestamp) {
  if (this.passwordChangedAt) {
    const changedTimestamp = parseInt(
      `${this.passwordChangedAt.getTime() / 1000}`,
      10,
    )

    return JWTTimestamp < changedTimestamp
  }

  return false
}

UserSchema.methods.createResetToken = function () {
  const resetToken = crypto.randomBytes(32).toString('hex')
  const hashedToken = UtilityService.createHashToken(resetToken)

  this.passwordResetToken = hashedToken
  this.passwordResetTokenExpires = Date.now() + 10 * 60 * 1000

  return resetToken
}

const User: Model<IUser> = mongoose.model('User', UserSchema)

export default User
