import { IPaystack } from '../utils/interface'
import mongoose, { Model, Schema } from 'mongoose'

const PaystackSchema: Schema<IPaystack> = new mongoose.Schema(
  {
    message: String,
    reference: String,
    status: String,
    transaction: String,
    laundry: {
      type: Schema.Types.ObjectId,
      ref: 'Laundry',
    },
  },
  { timestamps: true },
)

PaystackSchema.pre(/^find/, function (next) {
  this.populate('laundry')
  next()
})

const Paystack: Model<IPaystack> = mongoose.model('Paystack', PaystackSchema)

export default Paystack
