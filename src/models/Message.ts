import { IMessage } from './../utils/interface'
import mongoose, { Model, Schema } from 'mongoose'
import validator from 'validator'

const MessageSchema: Schema<IMessage> = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please enter your name'],
      trim: true
    },

    email: {
      type: String,
      validate: [validator.isEmail, 'Please enter a valid email'],
    },

    message: {
      type: String,
      required: [true, 'Please enter your message'],
      trim: true
    },
  },
  { timestamps: true },
)

const Message: Model<IMessage> = mongoose.model('Message', MessageSchema)

export default Message
