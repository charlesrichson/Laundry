import { IPrices } from '../utils/interface'
import mongoose, { Model, Schema } from 'mongoose'

const PricesSchema: Schema<IPrices> = new mongoose.Schema(
  {
    washDrySelf: Number,
    drySelf: Number,
    washDryDropoff: Number,
    dryDropOff: Number,
    ironDropoff: Number,
    iron: Number,
    detergent: Number,
    bleach: Number,
    softner: Number,
    stainRemover: Number,
    delivery: Number,
    laundryBag: Number,
    ironing: [
      {
        name: String,
        key: String,
        price: Number,
      },
    ],
  },
  { timestamps: true },
)

const Prices: Model<IPrices> = mongoose.model('Prices', PricesSchema)

export default Prices
