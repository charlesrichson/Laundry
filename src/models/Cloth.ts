import { ICloth } from './../utils/interface'
import mongoose, { Schema, Model } from 'mongoose'

const ClothSchema: Schema<ICloth> = new mongoose.Schema(
  {
    cloth: {
      type: String,
      required: [true, 'Please enter a cloth'],
      unique: true,
      trim: true,
    },

    key: String,
  },
  { timestamps: true },
)

const Cloth: Model<ICloth> = mongoose.model('Cloth', ClothSchema)

export default Cloth
