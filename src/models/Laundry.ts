import mongoose, { Model, Schema } from 'mongoose'
import { ILaundry } from '../utils/interface'

export enum ServiceType {
  self = 'self',
  dropoff = 'dropoff',
  delivery = 'delivery',
}

export enum PaymentMethod {
  cash = 'cash',
  pos = 'pos',
  transfer = 'transfer',
  card = 'card',
}

export enum Progress {
  pending = 'pending',
  washing = 'washing',
  delivered = 'delivered',
  cancelled = 'cancelled',
}

const LaundrySchema: Schema<ILaundry> = new mongoose.Schema(
  {
    service: {
      type: String,
      required: [true, 'Select a Laundry Service'],
    },

    email: {
      type: String,
      required: [true, 'Add user email'],
    },

    user: {
      name: String,
      phone: Number,
      photo: String,
      address: String,
      email: String,
    },

    serviceType: {
      type: String,
      enum: ServiceType,
      required: [true, 'Select a Service Type'],
    },

    serviceQty: {
      type: Number,
      default: 1,
    },

    iron: {
      type: Boolean,
      default: false,
    },

    laundryDetails: [
      {
        fabric: String,
        count: Number,
      },
    ],

    totalCount: {
      type: Number,
      default: 0,
    },

    detergent: {
      type: Boolean,
      default: true,
    },

    detergentQty: {
      type: Number,
      default: 1,
    },

    laundryBag: {
      type: Boolean,
      default: true,
    },

    laundryBagQty: {
      type: Number,
      default: 1,
    },

    extraCareOpt: [{ product: String, quantity: Number }],

    branchLocation: {
      type: String,
      required: [true, 'Please add a location'],
    },

    location: {
      type: String,
      required: [true, 'Please add a location'],
    },

    isPaid: {
      type: Boolean,
      default: false,
    },

    paidAt: Date,

    paymentDetails: {},

    isDelivered: {
      type: Boolean,
      default: false,
    },

    deliveredAt: Date,

    isPicked: {
      type: Boolean,
      default: false,
    },

    pickedAt: Date,

    paymentMethod: {
      type: String,
      enum: PaymentMethod,
      default: PaymentMethod.cash,
    },

    progress: {
      type: String,
      enum: Progress,
      default: Progress.pending,
    },

    subTotal: {
      type: Number,
      required: true,
    },

    total: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true },
)

const Laundry: Model<ILaundry> = mongoose.model('Laundry', LaundrySchema)

export default Laundry
