import mongoose, { Model, Schema } from 'mongoose'
import { IAttendance } from '../utils/interface'

const AttendanceSchema: Schema<IAttendance> = new mongoose.Schema(
  {
    employee: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: [true, 'Please select an employee'],
    },

    date: {
      type: [Date],
      required: [true, 'Add the current time'],
    },
  },
  { timestamps: true },
)

const Attendance: Model<IAttendance> = mongoose.model(
  'Attendance',
  AttendanceSchema,
)

export default Attendance
