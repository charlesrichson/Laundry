import mongoose, { Model, Schema } from 'mongoose'
import { IExpoToken } from '../utils/interface'

const ExpoTokenSchema: Schema<IExpoToken> = new mongoose.Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: [true, 'Please select a user'],
    },

    pushToken: {
      type: String,
      required: [true, 'Please input pushToken'],
    },

    isSubscribed: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true },
)

const ExpoToken: Model<IExpoToken> = mongoose.model(
  'ExpoToken',
  ExpoTokenSchema,
)

export default ExpoToken
