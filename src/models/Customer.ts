import { ICustomer } from './../utils/interface'
import mongoose, { Schema, Model } from 'mongoose'
import validator from 'validator'

const CustomerSchema: Schema<ICustomer> = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Please enter a name'],
      trim: true,
    },

    email: {
      type: String,
      unique: true,
      required: [true, 'Please enter an email'],
      lowercase: true,
      validate: [validator.isEmail, 'Please enter a valid email'],
    },

    phone: {
      type: Number,
      required: [true, 'Please enter a phone number'],
    },

    points: {
      type: Number,
      default: 0.0,
    },

    address: String,
  },
  { timestamps: true },
)

const Customer: Model<ICustomer> = mongoose.model('Customer', CustomerSchema)

export default Customer
