import { ISettings } from '../utils/interface'
import mongoose, { Schema, Model } from 'mongoose'

const SettingsSchema: Schema<ISettings> = new mongoose.Schema(
  {
    branches: [String],

    attendant: {
      type: [Schema.Types.ObjectId],
      ref: 'User',
    },

    dispatch: {
      type: [Schema.Types.ObjectId],
      ref: 'User',
    },

    manager: {
      type: [Schema.Types.ObjectId],
      ref: 'User',
    },
  },
  { timestamps: true },
)

const Settings: Model<ISettings> = mongoose.model('Settings', SettingsSchema)

export default Settings
